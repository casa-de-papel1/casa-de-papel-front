import {Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { MonthMovements } from '../../shared/monthMovements';
import { Month } from '../../shared/month';
import { MonthsMovementsService } from '../../services/months-movements.service';

@Component({
  selector: 'app-choose-month',
  templateUrl: './choose-month.component.html',
  styleUrls: ['./choose-month.component.css']
})
export class ChooseMonthComponent implements OnInit, OnChanges {
  @Input() monthsMovements: MonthMovements[];
  @Input() tabChanged: Event;
  months: Month[];
  selectedMonth: number;

  constructor(private monthsMovementsService: MonthsMovementsService) { }

  ngOnInit(): void {
    // we want to sort the months from later to earlier
    this.monthsMovements.sort((first, second) => second.month - first.month);
    // update the selected month to be the last one every time tab is changed
    this.selectedMonth = this.monthsMovementsService.getLastMonthNum(this.monthsMovements);
    this.months = this.monthsMovementsService.getMonthsNames(this.monthsMovements);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  onMonthChange(event): void {
    this.monthsMovementsService.updateSelectedMonth(event.value);
  }
}

