// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexModule } from '@angular/flex-layout';
// Components
import { AppComponent } from './app.component';
import { ChooseMonthComponent } from './components/choose-month/choose-month.component';
import { IncomeVsOutcomeComponent } from './components/income-vs-outcome/income-vs-outcome.component';
import { MainComponent } from './components/main/main.component';
import { MonthSumInfoComponent } from './components/month-sum-info/month-sum-info.component';
import { MovementsListComponent } from './components/movements-list/movements-list.component';
import { ReportComponent } from './components/report/report.component';


import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    ChooseMonthComponent,
    IncomeVsOutcomeComponent,
    MainComponent,
    MonthSumInfoComponent,
    MovementsListComponent,
    ReportComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexModule,
    FormsModule,
    MatTableModule,
    MatCardModule,
    MatTabsModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
