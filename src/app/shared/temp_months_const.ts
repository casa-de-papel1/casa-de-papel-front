import { MonthMovements } from './monthMovements';

export const MONTHS_DATA: MonthMovements[] = [
  {
    month: 8,
    movements: [
      {
        date: '2020-08-02',
        description: '4569 - כרטיסי אשראי לי',
        credit: ' ',
        debt: '244.5',
        balance: ' '
      },
      {
        date: '2020-08-02',
        description: '7506 - ישראכרט',
        credit: ' ',
        debt: '730.6',
        balance: ' '
      },
      {
        date: '2020-08-02',
        description: '6567 - ישראכרט',
        credit: ' ',
        debt: '1956.41',
        balance: ' '
      },
      {
        date: '2020-08-02',
        description: 'ישראכרט',
        credit: ' ',
        debt: '240',
        balance: ' '
      },
      {
        date: '2020-08-02',
        description: 'אוניברסיטת בר א',
        credit: '2904',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-08-02',
        description: '\"משכורת\"',
        credit: '15492.3',
        debt: ' ',
        balance: '253493.27'
      },
      {
        date: '2020-08-06',
        description: 'זיכוי מב.פועלים',
        credit: '1000',
        debt: ' ',
        balance: '254493.27'
      },
      {
        date: '2020-08-10',
        description: 'משכורת',
        credit: '1725.1',
        debt: ' ',
        balance: '256218.37'
      },
      {
        date: '2020-08-12',
        description: 'זיכוי מב.פועלים',
        credit: '750',
        debt: ' ',
        balance: '256968.37'
      },
    ]
  },
  {
    month: 7,
    movements: [
      {
        date: '2020-07-01',
        description: 'אוניברסיטת בר א',
        credit: '621.36',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-07-01',
        description: '\"משכורת\"',
        credit: '15846.64',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-07-01',
        description: 'מ.מ. שהם',
        credit: '249.45',
        debt: ' ',
        balance: '240823.17'
      },
      {
        date: '2020-07-02',
        description: '7506 - ישראכרט',
        credit: ' ',
        debt: '962.74',
        balance: ' '
      },
      {
        date: '2020-07-02',
        description: '6567 - ישראכרט',
        credit: ' ',
        debt: '2498.66',
        balance: ' '
      },
      {
        date: '2020-07-02',
        description: 'ישראכרט',
        credit: ' ',
        debt: '565.826',
        balance: '236795.95'
      },
      {
        date: '2020-07-08',
        description: 'זיכוי מב.פועלים',
        credit: '22',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-07-08',
        description: 'זיכוי מב.פועלים',
        credit: '22',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-07-08',
        description: 'זיכוי מב.פועלים',
        credit: '22',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-07-08',
        description: 'זיכוי מב.פועלים',
        credit: '44',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-07-10',
        description: 'משכורת',
        credit: '1010.77',
        debt: ' ',
        balance: '237916.72'
      },
      {
        date: '2020-07-12',
        description: 'זיכוי מב.פועלים',
        credit: '285',
        debt: ' ',
        balance: '238201.72'
      },
      {
        date: '2020-07-19',
        description: '6567 - ישראכרט',
        credit: '26.76',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-07-19',
        description: 'זיכוי מב.פועלים',
        credit: '40',
        debt: ' ',
        balance: '238268.48'
      },
    ]
  },
  {
    month: 6,
    movements: [
      {
        date: '2020-06-01',
        description: '\"משכורת\"',
        credit: '14431.57',
        debt: '',
        balance: '238327.72'
      },
      {
        date: '2020-06-02',
        description: '4569 - כרטיסי אשראי לי',
        credit: '',
        debt: '79.9',
        balance: ''
      },
      {
        date: '2020-06-02',
        description: '6567 - ישראכרט',
        debt: '895.02',
        credit: '',
        balance: ''
      },
      {
        date: '2020-06-02',
        description: '7506 - ישראכרט',
        debt: '1594.63',
        credit: '',
        balance: ''
      },
      {
        date: '2020-06-02',
        description: 'ישראכרט',
        debt: '655.95',
        credit: '',
        balance: ''
      },
      {
        date: '2020-06-02',
        description: 'זיכוי מב.פועלים',
        credit: '30',
        debt: '',
        balance: ''
      },
      {
        date: '2020-06-02',
        description: 'זיכוי מב.פועלים',
        credit: '30',
        debt: '',
        balance: ''
      },
      {
        date: '2020-06-02',
        description: 'זיכוי מב.פועלים',
        credit: '30',
        debt: '',
        balance: ''
      },
      {
        date: '2020-06-02',
        description: 'זיכוי מב.פועלים',
        credit: '30',
        debt: '',
        balance: '235222.22'
      },
      {
        date: '2020-06-03',
        description: 'זיכוי מב.פועלים',
        credit: '21',
        debt: '',
        balance: ''
      },
      {
        date: '2020-06-03',
        description: 'זיכוי מב.פועלים',
        credit: '21',
        debt: '',
        balance: ''
      },
      {
        date: '2020-06-03',
        description: 'זיכוי מב.פועלים',
        credit: '21',
        debt: '',
        balance: '235285.22'
      },
      {
        date: '2020-06-10',
        description: 'משכורת',
        credit: '1790.5',
        debt: '',
        balance: '237075.72'
      },
      {
        date: '2020-06-12',
        description: 'העברה מהחשבון',
        debt: '13000',
        credit: '',
        balance: '224075.72'
      },
      {
        date: '2020-06-17',
        description: 'זיכוי מב.פועלים',
        credit: '30',
        debt: ' ',
        balance: '224105.72'
      },
    ]
  },
  {
    month: 5,
    movements: [
      {
        date: '2020-05-01',
        description: '\"משכורת\"',
        credit: '14816.87',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-05-01',
        description: 'מ.מ. שהם',
        credit: '220.2',
        debt: ' ',
        balance: '225046.66'
      },
      {
        date: '2020-05-03',
        description: '4569 - כרטיסי אשראי לי',
        credit: ' ',
        debt: '69.9',
        balance: ' '
      },
      {
        date: '2020-05-03',
        description: '6567 - ישראכרט',
        credit: ' ',
        debt: '453.74',
        balance: ' '
      },
      {
        date: '2020-05-03',
        description: '7506 - ישראכרט',
        credit: ' ',
        debt: '1023.35',
        balance: ' '
      },
      {
        date: '2020-05-03',
        description: 'ישראכרט',
        credit: ' ',
        debt: '1030.49',
        balance: '222469.18'
      },
      {
        date: '2020-05-10',
        description: 'משכורת',
        credit: '1175.97',
        debt: ' ',
        balance: '223645.15'
      },
      {
        date: '2020-05-11',
        description: 'זיכוי מב.פועלים',
        credit: '84',
        debt: ' ',
        balance: ' '
      },
      {
        date: '2020-05-11',
        description: 'זיכוי מב.פועלים',
        credit: '167',
        debt: ' ',
        balance: '223896.15'
      }
    ]
  }
];

