export class LocalExpense {
  date: string;
  businessName: string;
  dealAmount: string;
  chargeAMount: string;
  description: string;
}
