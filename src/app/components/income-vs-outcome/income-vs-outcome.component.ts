import { Component, OnInit, Input } from '@angular/core';
import { InVsOutConfig } from '../../shared/config';
import { Chart } from 'chart.js';
import { MonthMovements } from '../../shared/monthMovements';
import { MonthsMovementsService } from '../../services/months-movements.service';
import { Movement } from '../../shared/movement';

@Component({
  selector: 'app-income-vs-outcome',
  templateUrl: './income-vs-outcome.component.html',
  styleUrls: ['./income-vs-outcome.component.css']
})
export class IncomeVsOutcomeComponent implements OnInit {
  @Input() monthsMovements: MonthMovements[];
  @Input() selectedMonth: number;
  inOutChart = document.getElementById('inOutChart');
  barChart: Chart;
  labels: string[];

  constructor(private monthsMovementsService: MonthsMovementsService) { }

  ngOnInit(): void {
    this.labels = this.getLabels();
    this.barChart = this.buildChart();
  }

  buildChart(): Chart {
    return new Chart('inOutChart', {
      type: InVsOutConfig.GRAPH_TYPE,
      data: {
        labels: this.labels,
        datasets: this.getDatasets()
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        tooltips: {
          titleAlign: 'center',
          bodyAlign: 'center',
          footerAlign: 'center',
          displayColors: false,
          callbacks: {
            label: (tooltipItem, data) => this.tooltipLabel(tooltipItem, data),
            afterLabel: (tooltipItem, data) => this.largestMovementsString(tooltipItem, data)
          }
        }
      }
    });
  }

  // tooltips functions

  largestMovementsString(tooltipItem, data): string {
    const monthName = tooltipItem.xLabel;
    // because the index starts from 0, we need to add 1
    const monthNum = InVsOutConfig.MONTHS_NAMES.indexOf(monthName) + 1;
    let res = '\n';
    // check if the bar is of incomes or outcomes
    if (data.datasets[tooltipItem.datasetIndex].label === InVsOutConfig.INCOMES_STR) {
      const sortedIncomes = this.sortIncomes(monthNum);
      // create the string of largest incomes
      for (let i = 0; i < InVsOutConfig.LARGEST_MOVEMENTS; i++) {
        res += sortedIncomes[i].description + ': ' + sortedIncomes[i].credit + '\n';
      }
    } else if (data.datasets[tooltipItem.datasetIndex].label === InVsOutConfig.OUTCOMES_STR) {
      const sortedOutcomes = this.sortOutcomes(monthNum);
      // create the string of largest outcomes
      for (let i = 0; i < InVsOutConfig.LARGEST_MOVEMENTS; i++) {
        res += sortedOutcomes[i].description + ': ' + sortedOutcomes[i].debt + '\n';
      }
    }
    return res;
  }

  sortIncomes(selectedMonth: number): Movement[] {
    const sortedIncomes = [];
    const selectedMonthIndex = this.monthsMovementsService.findMonthIndex(this.monthsMovements, selectedMonth);
    const length = this.monthsMovements[selectedMonthIndex].movements.length;
    for (let i = 0; i < length; i++) {
      // if credit is blank then the movement is an outcome
      if (!InVsOutConfig.BLANKS.includes(this.monthsMovements[selectedMonthIndex].movements[i].credit)) {
        sortedIncomes.push(this.monthsMovements[selectedMonthIndex].movements[i]);
      }
    }
    sortedIncomes.sort((a, b) => +b.credit - +a.credit);
    return sortedIncomes;
  }

  sortOutcomes(selectedMonth: number): Movement[] {
    const sortedOutcomes = [];
    const selectedMonthIndex = this.monthsMovementsService.findMonthIndex(this.monthsMovements, selectedMonth);
    const length = this.monthsMovements[selectedMonthIndex].movements.length;
    for (let i = 0; i < length; i++) {
      // if debt is blank then the movement is an income
      if (!InVsOutConfig.BLANKS.includes(this.monthsMovements[selectedMonthIndex].movements[i].debt)) {
        sortedOutcomes.push(this.monthsMovements[selectedMonthIndex].movements[i]);
      }
    }
    sortedOutcomes.sort((a, b) => +b.debt - +a.debt);
    return sortedOutcomes;
  }

  tooltipLabel(tooltipItem, data): string {
    return data.datasets[tooltipItem.datasetIndex].label + ': ' + tooltipItem.yLabel.toFixed(2);
  }

  // datasets functions

  getDatasets(): any[]{
    const datasets = [];
    const incomes = {
      label: InVsOutConfig.INCOMES_STR,
      backgroundColor: InVsOutConfig.INCOME_COLOR,
      borderWidth: InVsOutConfig.BORDER_WIDTH,
      borderColor: InVsOutConfig.BORDER_COLOR,
      hoverBorderWidth: InVsOutConfig.HOVER_BORDER_WIDTH,
      hoverBorderColor: InVsOutConfig.HOVER_BORDER_COLOR,
      data: this.getAllMonthsIncomes()
    };
    datasets.push(incomes);
    const outcomes = {
      label: InVsOutConfig.OUTCOMES_STR,
      backgroundColor: InVsOutConfig.OUTCOME_COLOR,
      borderWidth: InVsOutConfig.BORDER_WIDTH,
      borderColor: InVsOutConfig.BORDER_COLOR,
      hoverBorderWidth: InVsOutConfig.HOVER_BORDER_WIDTH,
      hoverBorderColor: InVsOutConfig.HOVER_BORDER_COLOR,
      data: this.getAllMonthsOutcomes()
    };
    datasets.push(outcomes);
    return datasets;
  }

  getAllMonthsIncomes(): number[] {
    const incomes = [];
    const length = this.monthsMovements.length;
    for (let i = 0; i < length; i++) {
      // push to array the total income of a specific month
      incomes.push(this.monthsMovementsService.getIncome(this.monthsMovements, this.monthsMovements[i].month));
    }
    return incomes;
  }

  getAllMonthsOutcomes(): number[] {
    const outcomes = [];
    const length = this.monthsMovements.length;
    for (let i = 0; i < length; i++) {
      // push to array the total outcome of a specific month
      outcomes.push(this.monthsMovementsService.getOutcome(this.monthsMovements, this.monthsMovements[i].month));
    }
    return outcomes;
  }

  getLabels(): string[] {
    const labels = [];
    const length = this.monthsMovements.length;
    for (let i = 0; i < length; i++) {
      // because the names array index starts from 0, we need to subtract 1
      labels.push(InVsOutConfig.MONTHS_NAMES[this.monthsMovements[i].month - 1]);
    }
    return labels;
  }
}

