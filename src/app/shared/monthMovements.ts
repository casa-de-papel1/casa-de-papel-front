import { Movement } from './movement';

export class MonthMovements {
  month: number;
  movements: Movement[];
}
