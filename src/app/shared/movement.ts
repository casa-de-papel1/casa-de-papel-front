export class Movement {
  date: string;
  description: string;
  credit: string;
  debt: string;
  balance: string;
}
