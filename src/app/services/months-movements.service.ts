import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MonthMovements } from '../shared/monthMovements';
import { Month } from '../shared/month';
import { MonthsMovementsServiceConfig } from '../shared/config';
import { MONTHS_DATA } from '../shared/temp_months_const';

@Injectable({
  providedIn: 'root'
})
export class MonthsMovementsService {

  // todo change name
  private defaultMonth = new BehaviorSubject<number>(this.getLastMonthNum(this.getMonthsMovements()));
  selectedMonth = this.defaultMonth.asObservable();

  constructor() { }

  getMonthsMovements(): MonthMovements[] {
    return MONTHS_DATA;
  }

  getMonthsNames(monthsMovements: MonthMovements[]): Month[] {
    const months: Month[] = [];
    const length = monthsMovements.length;
    for (let i = 0; i < length; i++) {
      // check if the month num is valid
      if (monthsMovements[i].month < MonthsMovementsServiceConfig.FIRST_MONTH ||
        monthsMovements[i].month > MonthsMovementsServiceConfig.LAST_MONTH) {
        throw new Error('Month num is invalid!');
      }
      const month = {
        num: monthsMovements[i].month,
        name: MonthsMovementsServiceConfig.MONTHS_NAMES[monthsMovements[i].month - 1]
      };
      months.push(month);
    }
    return months;
  }

  getLastMonthNum(monthsMovements: MonthMovements[]): number {
    return monthsMovements[0].month;
  }

  findMonthIndex(monthsMovements: MonthMovements[], selectedMonth: number): number {
    let selectedIndex = 0;
    const length = monthsMovements.length;
    for (selectedIndex; selectedIndex < length; selectedIndex++) {
      if (monthsMovements[selectedIndex].month === selectedMonth) {
        return selectedIndex;
      }
    }
    return selectedIndex - 1;
  }

  getIncome(monthsMovements: MonthMovements[], selectedMonth: number): number {
    const selectedIndex = this.findMonthIndex(monthsMovements, selectedMonth);
    // now calculate the income of this month
    let income = 0;
    const movementsAmount = monthsMovements[selectedIndex].movements.length;
    for (let i = 0; i < movementsAmount; i++) {
      // if debt is blank then the credit is not blank
      if (MonthsMovementsServiceConfig.BLANKS.includes( monthsMovements[selectedIndex].movements[i].debt)) {
        income += +monthsMovements[selectedIndex].movements[i].credit;
      }
    }
    return income;
  }

  getOutcome(monthsMovements: MonthMovements[], selectedMonth: number): number {
    const selectedIndex = this.findMonthIndex(monthsMovements, selectedMonth);
    // now calculate the income of this month
    let outcome = 0;
    const movementsAmount = monthsMovements[selectedIndex].movements.length;
    for (let i = 0; i < movementsAmount; i++) {
      // if credit is blank then the debt is not blank
      if (MonthsMovementsServiceConfig.BLANKS.includes(monthsMovements[selectedIndex].movements[i].credit)) {
        outcome += +monthsMovements[selectedIndex].movements[i].debt;
      }
    }
    return outcome;
  }

  getBalance(monthsMovements: MonthMovements[], selectedMonth: number): string {
    const selectedIndex = this.findMonthIndex(monthsMovements, selectedMonth);
    const length = monthsMovements[selectedIndex].movements.length;
    for (let i = length - 1; i >= 0; i--) {
      if (!MonthsMovementsServiceConfig.BLANKS.includes(monthsMovements[selectedIndex].movements[i].balance)) {
        return monthsMovements[selectedIndex].movements[i].balance;
      }
    }
    // default value
    return MonthsMovementsServiceConfig.UNKNOWN_BALANCE;
  }

  updateSelectedMonth(monthNum: number): void {
    this.defaultMonth.next(monthNum);
  }
}
