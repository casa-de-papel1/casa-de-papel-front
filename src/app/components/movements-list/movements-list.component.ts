import {Component, OnInit, Input, SimpleChanges, OnChanges} from '@angular/core';
import { MonthsMovementsService } from '../../services/months-movements.service';
import { Movement } from '../../shared/movement';
import { MonthMovements } from '../../shared/monthMovements';
import { MovementsListConfig } from '../../shared/config';

@Component({
  selector: 'app-movements-list',
  templateUrl: './movements-list.component.html',
  styleUrls: ['./movements-list.component.css']
})
export class MovementsListComponent implements OnInit, OnChanges {
  @Input() monthsMovements: MonthMovements[];
  @Input() selectedMonth: number;
  columns: string[];
  selectedMovements: Movement[];
  constructor(private monthsMovementsService: MonthsMovementsService) { }

  ngOnInit(): void {
    this.columns = MovementsListConfig.COLUMNS;
    this.selectedMovements = this.getSelectedMovements();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  onMonthChange(monthNum: number): void {
    console.log(this.selectedMonth);
    this.selectedMonth = monthNum;
    console.log(this.selectedMonth);
    // this.ngOnInit();
  }

  getSelectedMovements(): Movement[] {
    const length = this.monthsMovements.length;
    for (let i = 0; i < length; i++) {
      if (this.monthsMovements[i].month === this.selectedMonth) {
        return this.monthsMovements[i].movements;
      }
    }
    // if selected month was not found in monthsMovements list
    throw new Error('Error! month was not found');
  }
  isCard(description): boolean {
    return description.includes('ישראכרט') || description.includes('אשראי'); // todo consts?
  }
}
