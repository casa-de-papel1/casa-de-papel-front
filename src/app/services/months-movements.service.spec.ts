import { TestBed } from '@angular/core/testing';

import { MonthsMovementsService } from './months-movements.service';

describe('MonthsMovementsService', () => {
  let service: MonthsMovementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MonthsMovementsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
