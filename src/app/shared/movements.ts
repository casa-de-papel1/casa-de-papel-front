import { Movement } from './movement';

export const MOVEMENTS: Movement[] = [
  {
    date: '2020-06-01',
    description: '\"משכורת\"',
    credit: '14431.57',
    debt: '',
    balance: '238327.72'
  },
  {
    date: '2020-06-02',
    description: '4569 - כרטיסי אשראי לי',
    credit: '',
    debt: '79.9',
    balance: ''
  },
  {
    date: '2020-06-02',
    description: '6567 - ישראכרט',
    debt: '895.02',
    credit: '',
    balance: ''
  },
  {
    date: '2020-06-02',
    description: '7506 - ישראכרט',
    debt: '1594.63',
    credit: '',
    balance: ''
  },
  {
    date: '2020-06-02',
    description: 'ישראכרט',
    debt: '655.95',
    credit: '',
    balance: ''
  },
  {
    date: '2020-06-02',
    description: 'זיכוי מב.פועלים',
    credit: '30',
    debt: '',
    balance: ''
  },
  {
    date: '2020-06-02',
    description: 'זיכוי מב.פועלים',
    credit: '30',
    debt: '',
    balance: ''
  },
  {
    date: '2020-06-02',
    description: 'זיכוי מב.פועלים',
    credit: '30',
    debt: '',
    balance: ''
  },
  {
    date: '2020-06-02',
    description: 'זיכוי מב.פועלים',
    credit: '30',
    debt: '',
    balance: '235222.22'
  },
  {
    date: '2020-06-03',
    description: 'זיכוי מב.פועלים',
    credit: '21',
    debt: '',
    balance: ''
  },
  {
    date: '2020-06-03',
    description: 'זיכוי מב.פועלים',
    credit: '21',
    debt: '',
    balance: ''
  },
  {
    date: '2020-06-03',
    description: 'זיכוי מב.פועלים',
    credit: '21',
    debt: '',
    balance: '235285.22'
  },
  {
    date: '2020-06-10',
    description: 'משכורת',
    credit: '1790.5',
    debt: '',
    balance: '237075.72'
  },
  {
    date: '2020-06-12',
    description: 'העברה מהחשבון',
    debt: '13000',
    credit: '',
    balance: '224075.72'
  },
  {
    date: '2020-06-17',
    description: 'זיכוי מב.פועלים',
    credit: '30',
    debt: '',
    balance: '224105.72'
  },
];
