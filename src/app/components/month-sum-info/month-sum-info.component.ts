import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { MonthMovements } from '../../shared/monthMovements';
import { MonthsMovementsService } from '../../services/months-movements.service';
import { MonthsSumInfoConfig } from '../../shared/config';

@Component({
  selector: 'app-month-sum-info',
  templateUrl: './month-sum-info.component.html',
  styleUrls: ['./month-sum-info.component.css']
})
export class MonthSumInfoComponent implements OnInit, OnChanges {
  @Input() monthsMovements: MonthMovements[];
  @Input() selectedMonth: number;
  selectedBalance: number | string;
  selectedSavings: number;
  averageSavings: number;

  constructor(private monthsMovementsService: MonthsMovementsService) { }

  ngOnInit(): void {
    this.selectedBalance = this.getBalance();
    this.selectedSavings = this.getSelectedSavings(this.selectedMonth);
    this.averageSavings = this.getAverageSavings();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  getSelectedSavings(selected: number): number {
    const income = this.monthsMovementsService.getIncome(this.monthsMovements, selected);
    const outcome = this.monthsMovementsService.getOutcome(this.monthsMovements, selected);
    return +(+income - +outcome).toFixed(2);
  }

  getAverageSavings(): number {
    let totalSavings = 0;
    const length = this.monthsMovements.length;
    for (let i = 0; i < length; i++) {
      const singleMonthSavings = this.getSelectedSavings(this.monthsMovements[i].month);
      totalSavings += singleMonthSavings;
    }
    return totalSavings / length;
  }

  getBalance(): string {
    const res = this.monthsMovementsService.getBalance(this.monthsMovements, this.selectedMonth);
    if (!isNaN((+res))) {
      // todo comma
      return MonthsSumInfoConfig.NIS + (+res).toFixed(2).toLocaleString();
    }
    return res;
  }
}
