export class ForeignExpense {
  date: string;
  businessName: string;
  originalAmount: string;
  coin: string;
  chargeAmount: string;
}
