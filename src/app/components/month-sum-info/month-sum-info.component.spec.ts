import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthSumInfoComponent } from './month-sum-info.component';

describe('MonthSumInfoComponent', () => {
  let component: MonthSumInfoComponent;
  let fixture: ComponentFixture<MonthSumInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthSumInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthSumInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
