import { Component, OnInit } from '@angular/core';
import { MonthMovements } from '../../shared/monthMovements';
import { MonthsMovementsService } from '../../services/months-movements.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  monthMovements: MonthMovements[];
  selectedMonth: number;
  tabChanged: Event;

  constructor(private monthsMovementsService: MonthsMovementsService) { }

  ngOnInit(): void {
    this.monthMovements = this.monthsMovementsService.getMonthsMovements();
    this.monthsMovementsService.selectedMonth.subscribe(monthNum => this.selectedMonth = monthNum);
  }

  onMonthChange(monthNum): void {
    this.selectedMonth = monthNum;
  }

  onTabChange(event): void {
    // when tab is changed, update the selected month to be the last month
    this.monthsMovementsService.updateSelectedMonth(this.monthsMovementsService.getLastMonthNum(this.monthMovements));
    this.tabChanged = event;
  }
}
