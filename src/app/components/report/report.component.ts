import { Component, OnInit, Input } from '@angular/core';
import { Movement } from '../../shared/movement';
import { MonthMovements } from '../../shared/monthMovements';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  @Input() monthsMovements: MonthMovements[];
  @Input() selectedMonth: number;
  selectedMovements: Movement[];
  constructor() { }

  ngOnInit(): void {
    this.selectedMovements = this.getSelectedMovements();
  }
  getSelectedMovements(): Movement[] {
    const length = this.monthsMovements.length;
    for (let i = 0; i < length; i++) {
      if (this.monthsMovements[i].month === this.selectedMonth) {
        return this.monthsMovements[i].movements;
      }
    }
    // if selected month was not found in monthsMovements list
    throw new Error('Error! month was not found');
  }
}
