export const MonthsMovementsServiceConfig = {
  MONTHS_NAMES: [
    'ינואר',
    'פברואר',
    'מרץ',
    'אפריל',
    'מאי',
    'יוני',
    'יולי',
    'אוגוסט',
    'ספטמבר',
    'אוקטובר',
    'נובמבר',
    'דצמבר'
  ],
  FIRST_MONTH: 1,
  LAST_MONTH: 12,
  BLANKS: ['', ' '],
  UNKNOWN_BALANCE: 'יתרה לא ידועה'
};

export const InVsOutConfig = {
  LARGEST_MOVEMENTS: 3,
  GRAPH_TYPE: 'bar',
  INCOME_COLOR: 'rgba(0, 204, 0, 0.6)',
  OUTCOME_COLOR: 'rgba(204, 0, 0, 0.6)',
  BORDER_WIDTH: 1,
  BORDER_COLOR: '#777',
  HOVER_BORDER_WIDTH: 2,
  HOVER_BORDER_COLOR: '#000',
  MONTHS_NAMES: [
    'ינואר',
    'פברואר',
    'מרץ',
    'אפריל',
    'מאי',
    'יוני',
    'יולי',
    'אוגוסט',
    'ספטמבר',
    'אוקטובר',
    'נובמבר',
    'דצמבר'
  ],
  INCOMES_STR: 'הכנסות',
  OUTCOMES_STR: 'הוצאות',
  BLANKS: ['', ' ']
};

export const MovementsListConfig = {
  COLUMNS: ['balance', 'debt', 'credit', 'description', 'date']
};

export const MonthsSumInfoConfig = {
  NIS: '₪'
};
