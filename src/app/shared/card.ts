import { LocalExpense } from './localExpense';
import { ForeignExpense } from './foreignExpense';

export class Card {
  cardNum: string;
  month: string;
  localSum: string;
  localExpenses: LocalExpense[];
  foreignSum: string;
  foreignExpenses: ForeignExpense[];
}
